package com.tli.orders.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Data
@Entity
@Table(name = "order_line_items")
public class OrderLineItems {

    @Id
    private Long order_id;

    @JsonBackReference(value = "name")
    @ManyToOne
    @JoinColumn(name = "order_id",  insertable = false, updatable = false)
    private Orders orders;

    private String number;

    private String name;

    @Min(value = 0, message = "Can't be less than 0")
    private long price;

    private String quantity;
    @JsonIgnore
    private String createdDate;
    @JsonIgnore
    private String createdBy;
    @JsonIgnore
    private String modifiedDate;
    @JsonIgnore
    private String modifiedBy;

    public OrderLineItems(){}

    public OrderLineItems(Long order_id, Orders orders, String number, String name, @Min(value = 0, message = "Can't be less than 0") long price, String quantity, String createdDate, String createdBy, String modifiedDate, String modifiedBy) {
        this.order_id = order_id;
        this.orders = orders;
        this.number = number;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.modifiedDate = modifiedDate;
        this.modifiedBy = modifiedBy;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OrderLineItems{");
        sb.append("order_id=").append(order_id);
        sb.append(", orders=").append(orders);
        sb.append(", number='").append(number).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", price=").append(price);
        sb.append(", quantity='").append(quantity).append('\'');
        sb.append(", createdDate='").append(createdDate).append('\'');
        sb.append(", createdBy='").append(createdBy).append('\'');
        sb.append(", modifiedDate='").append(modifiedDate).append('\'');
        sb.append(", modifiedBy='").append(modifiedBy).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}

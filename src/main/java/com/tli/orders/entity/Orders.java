package com.tli.orders.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "orders")
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonManagedReference(value = "name")
    @OneToMany(mappedBy = "orders", fetch = FetchType.LAZY)
    private List<OrderLineItems> items;

    private String statusId;
    @JsonIgnore
    private String createdDate;
    @JsonIgnore
    private String createdBy;
    @JsonIgnore
    private String modifiedDate;
    @JsonIgnore
    private String modifiedBy;

    public Orders(){}

    public Orders(Long id, List<OrderLineItems> items, String statusId, String createdDate, String createdBy, String modifiedDate, String modifiedBy) {
        this.id = id;
        this.items = items;
        this.statusId = statusId;
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.modifiedDate = modifiedDate;
        this.modifiedBy = modifiedBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<OrderLineItems> getItems() {
        return items;
    }

    public void setItems(List<OrderLineItems> items) {
        this.items = items;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}

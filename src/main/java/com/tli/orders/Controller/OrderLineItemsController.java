package com.tli.orders.Controller;

import com.tli.orders.Repository.OrderLineItemsRepository;
import com.tli.orders.Repository.OrdersRespository;
import com.tli.orders.entity.OrderLineItems;
import com.tli.orders.entity.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class OrderLineItemsController {

    @Autowired
    private OrderLineItemsRepository orderLineItemsRepository;
    @Autowired
    private OrdersRespository ordersRespository;

    public OrderLineItemsController(OrderLineItemsRepository orderLineItemsRepository){
        this.orderLineItemsRepository=orderLineItemsRepository;
    }

    public OrderLineItems addItem(OrderLineItems orderLineItems){
        return  orderLineItemsRepository.save(orderLineItems);
    }

    //Retrieves all orderLineItems
    @GetMapping(path ="/orderLineItems",  produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OrderLineItems> viewAllOrders(){
        return  orderLineItemsRepository.findAll();
    }

    //Retrieves a single orderLineItem with id
    @GetMapping(path ="/orderLineItems/{id}",  produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<OrderLineItems> viewOrder(@PathVariable Long id ){
        return orderLineItemsRepository.findById(id);
    }

    //Creates a new order with array of orderlineitems
    @PostMapping(path = "/orderLineItems",
                consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<OrderLineItems>> placeOrder(@RequestBody List<OrderLineItems> newOrderLineItems) {

        int i = 1;
        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());

        for(OrderLineItems item : newOrderLineItems){
            Orders orders = new Orders();
            orders.setStatusId("1");
            orders.setModifiedDate(date.toString());
            orders.setCreatedDate(date.toString());
            orders.setCreatedBy("1");
            orders.setModifiedBy("1");
            ordersRespository.save(orders);

            String number = String.valueOf(i);
            item.setOrder_id(orders.getId());
            item.setNumber(number);
            item.setModifiedDate(date.toString());
            item.setCreatedDate(date.toString());
            item.setCreatedBy("1");
            item.setModifiedBy("1");
            i++;

            orderLineItemsRepository.save(item);
        }
        return new ResponseEntity<List<OrderLineItems>>(newOrderLineItems, HttpStatus.OK);
    }

    //Updates field quantity
    @PutMapping(path = "/orderLineItems/{id}")
    public OrderLineItems changeQuantity(@RequestBody OrderLineItems newOrderLineItems, @PathVariable Long id){

        return orderLineItemsRepository.findById(id)
                .map(updatedOrderItem -> {
                    updatedOrderItem.setQuantity(newOrderLineItems.getQuantity());
                    return orderLineItemsRepository.save(updatedOrderItem);
                })
                .orElseGet(()-> {
                   return orderLineItemsRepository.save(newOrderLineItems);
                });
    }

    //Removes a single line item
    @DeleteMapping(path = "/orderLineItems/{id}")
    public void removeLineItem(@PathVariable Long id){
        orderLineItemsRepository.deleteById(id);
    }

}

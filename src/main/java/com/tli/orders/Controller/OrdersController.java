package com.tli.orders.Controller;

import com.tli.orders.Repository.OrdersRespository;
import com.tli.orders.entity.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
public class OrdersController {

    @Autowired
    private OrdersRespository ordersRespository;

    public OrdersController(OrdersRespository ordersRespository){
        this.ordersRespository= ordersRespository;
    }

    //Retrieves all orders
    @GetMapping("/orders")
    public List<Orders> all(){
        return StreamSupport
                .stream(ordersRespository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    //Creates a single new orders (only in the order table)
    @PostMapping(path = "/orders",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Orders newOrder(@RequestBody Orders orders){
        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());

        orders.setModifiedDate(date.toString());
        orders.setCreatedDate(date.toString());
        orders.setCreatedBy("1");
        orders.setModifiedBy("1");
        return ordersRespository.save(orders);
    }

}

package com.tli.orders.Controller;

import com.tli.orders.Repository.OrderStatusesRepository;
import com.tli.orders.entity.OrderStatuses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderStatusesController {

    private OrderStatusesRepository orderStatusesRepository;

    public OrderStatusesController(OrderStatusesRepository orderStatusesRepository){
        this.orderStatusesRepository = orderStatusesRepository;
    }

    @GetMapping("/orderStatuses")
    List<OrderStatuses> all(){
        return orderStatusesRepository.findAll();
    }
}

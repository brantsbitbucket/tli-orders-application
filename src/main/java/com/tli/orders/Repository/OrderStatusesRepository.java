package com.tli.orders.Repository;

import com.tli.orders.entity.OrderStatuses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderStatusesRepository extends JpaRepository<OrderStatuses, Long> {
}

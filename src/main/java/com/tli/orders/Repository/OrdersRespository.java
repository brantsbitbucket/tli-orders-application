package com.tli.orders.Repository;

import com.tli.orders.entity.Orders;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdersRespository extends CrudRepository<Orders, Long> {
}
